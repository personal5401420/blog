# What's this?

This is my personal blog 'cuz I'm too lazy to set up my own Markdown blog thingy lol.

# Who are you?

I'm known as CIOSAI, CIOSAI_tw or jy. I make video game, music, generative art, font and more.

[![Youtube Icon](https://www.youtube.com/s/desktop/28bb7000/img/favicon_32x32.png)](https://youtube.com/@ciosai_tw?si=fzOsuP8yGvObEPPl)
[![Twitter Icon](https://abs.twimg.com/favicons/twitter.3.ico)](https://twitter.com/CIOSAI_tw)
[![Mastodon Icon](https://mastodon.gamedev.place/packs/media/icons/favicon-32x32-249409a6d9f300112c51af514d863112.png)](https://mastodon.gamedev.place/@CIOSAI_tw)
[![Itch.io Icon](https://itch.io/favicon.ico)](https://ciosai.itch.io/)

# Can I quote you?

Sure, and the charts too. Just remember to credit me by including the full link to the page where it came from.

# Can I use your code?

Absolutely! Please credit me and link back to the page where you found it. Except if you learn how it works and rewriting your own version, that's yours now. 😉
