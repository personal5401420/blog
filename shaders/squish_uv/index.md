# Squishing the UV

In this blog you will learn how to create this effect.

![UV being squished around like pinching a really soft dough and dragging the hand](./static/squishing_uv_test.gif)_X and Y axis being squished_

It's like if you were to pinch a point and drag it along.

I used it in [my PC demo](https://www.youtube.com/watch?v=pBReNjLqXAk) for [Nordlicht](https://nordlicht.demoparty.info/) 2024 at Bremen to create this "content-awareness" meme-y effect.

![Large text saying "Taiwan" being repeatedly squished](./static/squish_uv_applied_repeatedly.gif)_uv repeatedly squished_

[[_TOC_]]

[_Bla bla bla, just take me to the code_](#final-code)

## How does this work

### Split the axis into 2 parts

(I added the rings to see the squishing more clearly)

To narrow down the problem, let's do X axis, we pinch middle-left `0.4` and drag it slightly to the right to `0.7`.

It should end up like this.

![Normalized UV after being squished](./static/fully_squished_uv.png)

A left part, a right part, scaled to the border (`0.0` and `1.0`).

Consider the unsquished uv.

![Normalized UV before being squished](./static/unsquished_uv.png)

We first want to know which part is on the left and which the right. This is just $if(x > b)$.

Let's color the right part as black so see how that looks like.

```glsl
void main(){
    ...
    float a=0.4;
    float b=0.7;
    if(uv.x>b){
        col = vec3(0.);
    }
    ...
}
```

![Normalized UV before being squished, this time the right part is black](./static/right_part_made_black.png)

### Stretch the 2 parts

Now we need to stretch out the left part so that its `0.4` is moved to `0.7`.

$$
f(x) = x \frac{0.4}{0.7}
$$

: [^1]

another words

```glsl
void main(){
    ...
    float a=0.4;
    float b=0.7;
>   if(uv.x<b){
>       uv.x = uv.x*a/b;
    }
    ...
}
```

![Left part scaled](./static/left_part_scaled.png)

Nice, now the right bit. It has `1.0-0.7` of space for the rest of `1.0-0.4`.

$$
f(x) = x \frac{1.0-0.4}{1.0-0.7}
$$

![The right part is correctly scaled but has the wrong position](./static/scale_right_part_without_translate.png)

We've got a problem. We scaled it twice as dense, which is correct. But that's not `0.4` on the squished x on the cut line, that's way off to the right.

To make things easier, let's translate before scaling.

$$
f(x) = (x-0.7) \frac{1.0-0.4}{1.0-0.7}
$$

![The right part is translated back to origin before scaling](./static/right_part_translated_then_scaled.png)

We can see that this is $f(0.7)=0.0$

Now we just need to shift it back, since `0.0`-`0.4` is on the left side already, we skip `0.4` ahead.

Notice this `0.4` is in scale of the right side, this means we apply the translation after the scaling.

$$
f(x) = (x-0.7) \frac{1.0-0.4}{1.0-0.7} + 0.4
$$

```glsl
void main(){
    ...
    float a=0.4;
    float b=0.7;
    if(uv.x<b){
        uv.x = uv.x*a/b;
    }
>   else{
>       uv.x = (uv.x-b)*(1.-a)/(1.-b) + a;
>   }
    ...
}
```

![Normalized UV after being squished](./static/fully_squished_uv.png)

Voila!

### Refactor

This is cool, but how do you animate it like I did?

Well, since $b$ is where we drag it to. When $b=a$ there would be no distortions.

$b \neq a \implies$ Yes distortion

$b = a \implies$ No distortion

So just interpolate the new $b$ between $a$ and old $b$.

```glsl
void main(){
    ...
    float a=0.4;
    float b=0.7;
>   b=mix(a,b,fract(uTime));
    if(uv.x<b){
        uv.x = uv.x*a/b;
    }
    else{
        uv.x = (uv.x-b)*(1.-a)/(1.-b) + a;
    }
    ...
}
```

Putting it all into one function we get:

```glsl
vec2 squish(vec2 p, float a, float b, bool isHor, float amt){
    b=mix(a,b,amt);
    if(isHor){
        p.x=p.x>b?(p.x-b)*(1.-a)/(1.-b)+a:p.x*a/b;
        // this ternary can be written like this

        // if(p.x>b){
        //     p.x=(p.x-b)*(1.-a)/(1.-b)+a;
        // }
        // else{
        //     p.x=p.x*a/b;
        // }
    }
    else{
        // swap out p.x for p.y
        p.y=p.y>b?(p.y-b)*(1.-a)/(1.-b)+a:p.x*a/b;
    }
    return p;
}
```

## Final Code

I prefer having less parameters to remember so I turned `float a, float b, bool isHor` into `vec3 cut`.

```glsl
vec2 squish(vec2 p, vec3 cut, float amt){
    cut.y=mix(cut.x,cut.y,amt);
    if(cut.z>0.){
        p.x=p.x>cut.y?(p.x-cut.y)*(1.-cut.x)/(1.-cut.y)+cut.x:p.x*cut.x/cut.y;
    }
    else{
        p.y=p.y>cut.y?(p.y-cut.y)*(1.-cut.x)/(1.-cut.y)+cut.x:p.y*cut.x/cut.y;
    }
    return p;
}
```

[^1]: $f(x)$ for squishing function
